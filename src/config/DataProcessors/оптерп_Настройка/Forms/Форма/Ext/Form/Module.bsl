﻿
#Область ОписаниеПеременных

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

#КонецОбласти

// BSLLS:MissingParameterDescription-off

#Область ОбработчикиСобытийФормы

// Предопределенный метод
// 
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	Если НЕ оптерп_Настройка.АдминистрированиеДоступно() Тогда
		Сообщение	= Новый СообщениеПользователю;
		// BSLLS:Typo-off - "техподдержку" не проверяем
		Сообщение.Текст	= НСтр("ru = 'Изменение настроек доступно только администраторам. Обратитесь в техподдержку.'", "");
		// BSLLS:Typo-on
		Сообщение.Сообщить(); 
		Отказ	= Истина;
		Возврат;
	КонецЕсли;
	
	КонфигурацияЗаполнитьНаСервере();
	ТаблицаПравкиЗаполнитьНаСервере();
	ЭлементыПараметрыУстановитьНаСервере();
	
КонецПроцедуры

#КонецОбласти

#Область ОбработчикиКомандФормы

// Записывает настройку и закрывает форму
// 
&НаКлиенте
Процедура ЗаписатьИЗакрыть(Команда)
	
	Настройка	= Новый Соответствие();
	Для Каждого ПравкиСтрока Из ТаблицаПравки Цикл
		Настройка.Вставить(ПравкиСтрока.Идентификатор, ПравкиСтрока.Включена);
	КонецЦикла;
	НастройкаСохранитьНаСервере(Настройка);
	
	ЭтотОбъект.Закрыть();
		
КонецПроцедуры

#КонецОбласти 

#Область ОбработчикиСобытийЭлементовШапкиФормы

#КонецОбласти 

#Область ОбработчикиСобытийЭлементовТаблицыФормыТаблицаПродажи

// При активации строки ТаблицаПродажи. Выводит 
&НаКлиенте
Процедура ТаблицаПравкиПриАктивизацииСтроки(Элемент)
	
	ДанныеТекущие	= Элементы.ТаблицаПравки.ТекущиеДанные;
	
	Если ДанныеТекущие <> Неопределено Тогда
		ЭтотОбъект.ПравкаТекст	= ДанныеТекущие.Текст;
		ЭтотОбъект.ПравкаМетоды	= ДанныеТекущие.Методы;
	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти 

// BSLLS:MissingParameterDescription-on

#Область СлужебныеПроцедурыИФункции

// Сохраняет настройку активности правок
//
// Параметры: 
// 	Настройка - Соответствие - Сохраняемая настройка
//
&НаСервереБезКонтекста
Процедура НастройкаСохранитьНаСервере(Настройка)
	
	Для Каждого Элемент Из Настройка Цикл
		оптерп_Настройка.ПравкаИспользовать(Элемент.Ключ, Элемент.Значение);
	КонецЦикла;
	
КонецПроцедуры // НастройкаСохранитьНаСервере 

// Устанавливает параметры элементов формы
//
// Параметры: 
//
&НаСервере
Процедура ЭлементыПараметрыУстановитьНаСервере()
	
	Совместимо	= оптерп_Конфигурация.Совместима();
	Элементы.ДекорацияКонфигурацияСовместимостьОтсутствует.Видимость	= НЕ Совместимо;
	Элементы.ТаблицаПравки.ТолькоПросмотр								= НЕ Совместимо;
	
КонецПроцедуры // ЭлементыПараметрыУстановитьНаСервере 

// Заполняет сведения о конфигурации
//
// Параметры: 
//
&НаСервере
Процедура КонфигурацияЗаполнитьНаСервере()
	
	ЭтотОбъект.КонфигурацияИмя		= оптерп_Конфигурация.Идентификатор();
	ЭтотОбъект.КонфигурацияВерсия	= оптерп_Конфигурация.Версия();
	
КонецПроцедуры // КонфигурацияЗаполнитьНаСервере 

// Заполняет ТаблицаПравки
//
// Параметры: 
//
&НаСервере
Процедура ТаблицаПравкиЗаполнитьНаСервере()
	
	ТаблицаПравки.Очистить();
	
	Версия	= ЭтотОбъект.КонфигурацияВерсия;
	
	Для Каждого Правка Из оптерп_Состав.Получить() Цикл
		
		Идентификатор	= оптерп_Описание.Идентификатор(Правка);
		ПравкиСтрока	= ТаблицаПравки.Добавить();
		ПравкиСтрока.Идентификатор	= оптерп_Описание.Идентификатор(Правка);
		ПравкиСтрока.Вид			= оптерп_Описание.Вид(Правка);
		ПравкиСтрока.Область		= оптерп_Описание.Область(Правка);
		ПравкиСтрока.ВерсияМин		= оптерп_Описание.ВерсияМин(Правка);
		ПравкиСтрока.ВерсияМакс		= оптерп_Описание.ВерсияМакс(Правка);
		ПравкиСтрока.Текст			= оптерп_Описание.Текст(Правка);
		ПравкиСтрока.Методы			= оптерп_Описание.Методы(Правка);
		ПравкиСтрока.Доступна		= КонфигурацияПравкаДоступна(Версия, ПравкиСтрока.ВерсияМин, ПравкиСтрока.ВерсияМакс);
		ПравкиСтрока.Включена		= ПравкиСтрока.Доступна И оптерп_Правка.Включена(Идентификатор);
		
	КонецЦикла;
	
КонецПроцедуры // ТаблицаПравкиЗаполнитьНаСервере 

// Истина, если версия правки совместима с конфигурацией
//
// Параметры: 
// 	Версия - Строка - Версия конфигурации
// 	ВерсияМин - Строка - Минималь допустимая версия
// 	ВерсияМакс - Строка - Максимально допустимая версия
//
// Возвращаемое значение: 
// 	Булево
//
&НаКлиентеНаСервереБезКонтекста
Функция КонфигурацияПравкаДоступна(Версия, ВерсияМин, ВерсияМакс)
	
	Возврат (ПустаяСтрока(ВерсияМин) 
			ИЛИ оптерп_Версия.Сравнить(Версия, ВерсияМин, СтрЧислоВхождений(ВерсияМин, ".") + 1) >= 0)
		И (ПустаяСтрока(ВерсияМакс) 
			ИЛИ оптерп_Версия.Сравнить(Версия, ВерсияМакс, СтрЧислоВхождений(ВерсияМакс, ".") + 1) <= 0);
	
КонецФункции // ПравкаВерсияСовместима 

#КонецОбласти

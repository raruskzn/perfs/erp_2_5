﻿#Если Сервер Или ТолстыйКлиентОбычноеПриложение Или ВнешнееСоединение Тогда

#Область ОбработчикиСобытий

&Вместо("ПриЗаписи")
Процедура оптерп_ПриЗаписи(Отказ, Замещение)
	
	// ::: +++ Проверка включения оптимизации.
	Если НЕ оптерп_Правка
		.Включена(оптерп_Идентификатор.Хозрасчетный_ПриЗаписи_ПроверкаДолгосрочныхОбязательств())
		Тогда	
		ПродолжитьВызов(Отказ, Замещение);	
		Возврат;
	КонецЕсли;
	// ::: +++

	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	СтруктураПроверкиИзмененияРегистра = Неопределено;
	Если Не Отказ 
		И ДополнительныеСвойства.Свойство("СтруктураПроверкиИзмененияРегистра", СтруктураПроверкиИзмененияРегистра) Тогда

		// ::: +++ Оптимизация. Нет необходимости делать проверку, если не включен учет по долгосрочной задолженности.
		ИспользуютсяДолгосрочныеСчета = Ложь;
		Если ЭтотОбъект.Количество() Тогда
			ПроводкаОрганизация = ЭтотОбъект[0].Организация;
			ПроводкаПериод		= ЭтотОбъект[0].Период;
			ИспользуютсяДолгосрочныеСчета = 
				оптерп_ХозрасчетныйПовтИсп.ИспользуетсяДелениеНаДолгосрочныеКраткосрочныеАктивыОбязательства(ПроводкаОрганизация
					, КонецМесяца(ПроводкаПериод));
		КонецЕсли;
		
		ПроверкаИзмененияРегистра = (ДополнительныеСвойства.Свойство("ПроверкаИзмененияРегистра", ПроверкаИзмененияРегистра)
			И ПроверкаИзмененияРегистра = Истина);
		
		Если ИспользуютсяДолгосрочныеСчета = Ложь И ПроверкаИзмененияРегистра = Ложь Тогда
			Возврат;
		Иначе
		// ::: +++
			ПродолжитьВызов(Отказ, Замещение);
		КонецЕсли;

	КонецЕсли;
	
КонецПроцедуры

#КонецОбласти

#КонецЕсли
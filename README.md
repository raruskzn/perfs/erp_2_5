# Оптимизации производительности типовой конфигурации 1С:ERP с версии 2.5.10

Расширение содержит исправления типовой конфигурации 1С:ERP с версии 2.5.10

Основано на на [шаблоне](https://gitlab.com/raruskzn/fixes/template) исправлений для типовых конфигураций

[Основной](https://gitlab.com/raruskzn/fixes/template) функционал расширения

## Состав исправлений

*Доступ к сайту документации **закрыт** для России, необходимо использовать proxy или vpn. Например расширение [FastProxy](https://chrome.google.com/webstore/detail/fastproxy-%D0%BE%D0%B1%D1%85%D0%BE%D0%B4-%D0%B1%D0%BB%D0%BE%D0%BA%D0%B8%D1%80%D0%BE%D0%B2%D0%BA/mkelkmkgljeohnaeehnnkmdpocfmkmmf) для Chrome*

[Документация](https://raruskzn.gitlab.io/fixes/erp_2_5_10/%D0%B8%D1%81%D0%BF%D0%B5%D1%80%D0%BF_%D0%98%D1%81%D0%BF%D1%80%D0%B0%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F.%D0%B8%D1%81%D0%BF%D0%B5%D1%80%D0%BF_%D0%A1%D0%BE%D1%81%D1%82%D0%B0%D0%B2.html) строится автоматически на основании общего модуля исперп_Состав

## Добавление правки

[Статья](https://gitlab.com/raruskzn/fixes/template/-/blob/main/add_fix.md) как добавить правку типового функционала в расширение

Для подключения к хранилищам написать письмо с просьбой на адреса sergav@rarus.ru или shmalevoz@gmail.com